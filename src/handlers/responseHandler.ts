export const response = (res: any, status: number,message: string, data?: any, error?: any) => {
   return res.status(status).send({
        status,
        message,
        data
    });
}
