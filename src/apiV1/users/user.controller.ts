import * as bcrypt from 'bcrypt';
import { Request, Response } from 'express';
import * as jwt from 'jwt-then';
import config from '../../config/config';
import * as responseHandler from '../../handlers/responseHandler';
import User from './user.model';
import UserService from './user.service';


export default class UserController {
  private userService = new UserService();

  public findAll = async (req: Request, res: Response): Promise<any> => {
    try {
      const userDetail = await this.userService.getUserDetail();
      if(!userDetail) {
        responseHandler.response(res, 404, 'User not found', []);
      }
      responseHandler.response(res, 200, 'find ', userDetail);
    } catch (error) {
      responseHandler.response(res, 500, 'error ', []);
    }   
  };

  // public findOne = async (req: Request, res: Response): Promise<any> => {
  //   try {
  //     const user = await User.findById(req.params.id, { password: 0 });
  //     if (!user) {
  //       return res.status(404).send({
  //         success: false,
  //         message: 'User not found',
  //         data: null
  //       });
  //     }

  //     res.status(200).send({
  //       success: true,
  //       data: user
  //     });
  //   } catch (err) {
  //     res.status(500).send({
  //       success: false,
  //       message: err.toString(),
  //       data: null
  //     });
  //   }
  // };

  // public update = async (req: Request, res: Response): Promise<any> => {
  //   const { name, lastName, email, password } = req.body;
  //   try {
  //     const userUpdated = await User.findByIdAndUpdate(
  //       req.params.id,
  //       {
  //         $set: {
  //           name,
  //           lastName,
  //           email,
  //           password
  //         }
  //       },
  //       { new: true }
  //     );
  //     if (!userUpdated) {
  //       return res.status(404).send({
  //         success: false,
  //         message: 'User not found',
  //         data: null
  //       });
  //     }
  //     res.status(200).send({
  //       success: true,
  //       data: userUpdated
  //     });
  //   } catch (err) {
  //     res.status(500).send({
  //       success: false,
  //       message: err.toString(),
  //       data: null
  //     });
  //   }
  // };

  // public remove = async (req: Request, res: Response): Promise<any> => {
  //   try {
  //     const user = await User.findByIdAndRemove(req.params.id);

  //     if (!user) {
  //       return res.status(404).send({
  //         success: false,
  //         message: 'User not found',
  //         data: null
  //       });
  //     }
  //     res.status(204).send();
  //   } catch (err) {
  //     res.status(500).send({
  //       success: false,
  //       message: err.toString(),
  //       data: null
  //     });
  //   }
  // };
}
